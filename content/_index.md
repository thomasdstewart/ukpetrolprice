---
title: "UK Petrol Prices"
featured_image: "/ExxonMobil_Baton_Rouge.jpg"
---
# Backstory
It's been just over 11 years since I did some statistics in 2011 on [Petrol Prices](https://www.stewarts.org.uk/project/petrolprice/). The short story is that there was a strong correlation between the price of brent crude oil and the price of petrol, leading to a formula to predict petrol prices based on the price of brent crude oil. This was not really surprising, as this is sort of "known", but was good to confirm this for myself. However I was never really happy with the data in Excel. Also over time all of the data sources used for the original have changed or gone.

### 2011 Graph
This is the main graph from my [previous analysis](https://www.stewarts.org.uk/project/petrolprice/).
![](https://www.stewarts.org.uk/project/petrolprice/brent_crude_vs_petrol.png)

# Analysis
Similar to last time, the first step was to find the data for: brent crude oil, historical exchange rates for gbp and usd, petrol prices and inflation data. This is the list of data sources I found:

 * https://www.eia.gov/dnav/pet/hist/RBRTED.htm
 * https://www.bankofengland.co.uk/boeapps/database/index.asp?first=yes&SectionRequired=I&HideNums=-1&ExtraInfo=true
 * https://www.gov.uk/government/statistics/weekly-road-fuel-prices
 * https://www.ons.gov.uk/economy/inflationandpriceindices/timeseries/l55o/mm23

Initially I used a [Makefile](https://gitlab.com/thomasdstewart/ukpetrolprice/-/blob/0a40c97e80e0a5f2ea7b9095517c418d7c570c3e/Makefile) to download and process the data, however it quickly grew into a monster, and ultimately I moved everything to a Python script. I creatively called this this [grapher.py](https://gitlab.com/thomasdstewart/ukpetrolprice/-/blob/main/grapher.py). This is released under the GPL 3 or later licence. It downloads the data, slightly massages it, joins all the data together, does some analysis and then creates the graphs. The intention is that this can be re-run in the future periodically to re-generate the graphs as ease, which will allow for showing the much higher petrol prices of June 2022. Currently this is done as part of the GitLab CI/CD process. Check the date listed as last update to see the data age.

It also exports the final dataset to a csv file called [prices.csv](prices.csv) in case anyone wants to make their own graphs without the need for messing with other data sources. There is also [prices-processed.csv](prices-processed.csv) which shows the numbers I calculated.

All of this is self contained in a repo: https://gitlab.com/thomasdstewart/ukpetrolprice/.

Also credit to [Adbar](https://commons.wikimedia.org/wiki/User:Adbar) for the top [picture](https://en.wikipedia.org/wiki/File:ExxonMobil_Baton_Rouge.jpg).

# Graphs

## Brent

### Europe brent spot price FOB (USD per barrel) over time
[![](brent.png)](brent.png)

### Europe brent spot price FOB (GBP per barrel) over time
[![](brentgbp.png)](brentgbp.png)

### Europe brent spot price FOB (GBP per barrel) over time adjusted for inflation
[![](brentgbpadj.png)](brentgbpadj.png)

## Petrol

### Ultra low sulphur unleaded petrol pump price (p/litre)
[![](petrol.png)](petrol.png)

### Ultra low sulphur unleaded petrol pump price (p/litre) excluding vat
[![](petrolexvat.png)](petrolexvat.png)

### Ultra low sulphur unleaded petrol pump price (p/litre) excluding vat and excluding duty
[![](petrolexvatexduty.png)](petrolexvatexduty.png)

## Tax and exchange rates
### VAT over time
[![](vat.png)](vat.png)

### USD2GBP over time
[![](usd2gbp.png)](usd2gbp.png)

### Ultra low sulphur unleaded petrol duty rate (p/litre)
[![](duty.png)](duty.png)

### Ultra low sulphur unleaded petrol duty rate (p/litre) adjusted for inflation over time
[![](dutyadj.png)](dutyadj.png)

## Brent vs Petrol

### Europe brent spot price FOB (GBP dollars per barrel) vs ultra low sulphur unleaded petrol pump price (p/litre)
[![](brentvspetrol.png)](brentvspetrol.png)

### Europe brent spot price FOB (GBP per barrel) adjusted for inflation vs ultra low sulphur unleaded petrol pump price (p/litre) excluding vat and excluding duty adjusted for inflation
[![](brentvspetroladj.png)](brentvspetroladj.png)

### Europe brent spot price FOB (GBP per barrel) adjusted for inflation vs ultra low sulphur unleaded petrol pump price (p/litre) excluding vat and excluding duty adjusted for inflation 2020-2022
[![](brentvspetroladj2020.png)](brentvspetroladj2020.png)

In the spirit of a crazy formula, putting a trendline on the 2020-2022 data, a general formula can be calculated.

# Remarks
Here are some remarks about the graphs that occurred to me:
 * In 2022-04-01 brent crude oil was the minimum of $14.86 USD per barrel.
 * In 2022-05-01 brent crude oil was the maximum of $87.74 USD per barrel.
 * Petrol was cheapest in 2003-06-01 and most expensive in 2022-05-01.
 * The 2022-06-01 figures will create new maximums.
 * GBP getting weaker after 2016 due to Brexit can been seen.
 * The increases in duty in 2011 that triggered my first analysis can be seen.
 * Duty has stayed the same for a long time.
 * The 5p drop in duty is comparatively large.
 * When inflation it taken into consideration duty has decreased.
 * There is still a correlation between brent crude oil and petrol.
 * It seems that since for the last few years petrol costs more to produce from same price of brent crude oil.
 * Petrol has doubled in price since I purchased my car.
 * I like graphs.
 * Some other interactive Javascript tool to create graphs from csv would be nice.

Last update: {{< builddate >}}

Build log: [grapherlog.txt](grapherlog.txt)
