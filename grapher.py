#!/usr/bin/env python3
#    UK Petrol Prices Grapher
#    Copyright (C) 2022-2025 Thomas Stewart <thomas@stewarts.org.uk>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import datetime
import functools
import io
import logging
import lxml.etree
import matplotlib.patches
import matplotlib.pyplot
import numpy
import pandas
import requests
import sys
from pprint import pprint as pp

logging.basicConfig(level=logging.INFO,
                    format="%(levelname)s %(asctime)s [%(process)d] %(message)s")

logging.info('getting brent data')
#https://www.eia.gov
#https://www.eia.gov/petroleum/data.php
#https://www.eia.gov/dnav/pet/pet_pri_spt_s1_d.htm
#https://www.eia.gov/dnav/pet/hist/RBRTED.htm
url = 'https://www.eia.gov/dnav/pet/hist_xls/RBRTEm.xls'
req = requests.get(url)
out = io.BytesIO(req.content)
brent = pandas.read_excel(out, sheet_name='Data 1', skiprows=2, names=['date', 'brent'])
brent = brent[(brent['date'] >= '2003-06-01')]
brent['date'] = brent['date'] - datetime.timedelta(days=14)
logging.info('brent data:\n %s' % brent)

logging.info('getting usd2gbp data')
#https://www.icaew.com/library/subject-gateways/financial-markets/knowledge-guide-to-exchange-rates/historical-exchange-rates
#https://www.bankofengland.co.uk/boeapps/database/index.asp?first=yes&SectionRequired=I&HideNums=-1&ExtraInfo=true
year = str(datetime.date.today().year)
url = 'https://www.bankofengland.co.uk/boeapps/database/fromshowcolumns.asp?Travel=NIxIRxSUx&FromSeries=1&ToSeries=50&DAT=RNG&FD=1&FM=Jan&FY=2000&TD=31&TM=Dec&TY=' + year + '&FNY=&CSVF=TT&html.x=101&html.y=33&C=5YE&Filter=N'
req = requests.get(url, headers = {'User-Agent': 'Mozilla/5.0'})
html = req.content.decode('utf-8')
html = io.StringIO(html)
doc = lxml.etree.parse(html, lxml.etree.HTMLParser())
#print(lxml.etree.tostring(doc, pretty_print=True).decode())

data = []
rows = doc.xpath('//table[@id="stats-table"]/tbody/tr')
for row in rows:
    date = row.xpath('td[1]')[0].text
    value = row.xpath('td[2]')[0].text.strip()
    data.append([date, value])

usd2gbp = pandas.DataFrame(data, columns=['date', 'usd2gbp'])
usd2gbp['date'] = pandas.to_datetime(usd2gbp['date'], format='%d %b %y')
usd2gbp['date'] = usd2gbp['date'] + datetime.timedelta(days=1)
usd2gbp = usd2gbp[(usd2gbp['date'] >= '2003-06-01')]
usd2gbp = usd2gbp[:-1]
logging.info('usd2gbp data:\n %s' % usd2gbp)

logging.info('getting fuel data url')
#https://www.gov.uk/government/statistics/weekly-road-fuel-prices
#Ultra low sulphur unleaded petrol Pump price (p/litre)
#Ultra low sulphur unleaded petrol Duty rate (p/litre)
#Ultra low sulphur unleaded petrol VAT (% rate) ULSP

url = 'https://www.gov.uk/government/statistics/weekly-road-fuel-prices'
req = requests.get(url, headers = {'User-Agent': 'Mozilla/5.0'})
html = req.content.decode('utf-8')
html = io.StringIO(html)
doc = lxml.etree.parse(html, lxml.etree.HTMLParser())
#print(lxml.etree.tostring(doc, pretty_print=True).decode())


url = doc.xpath('//a[text()="Weekly road fuel prices (Excel)"]')[0].get('href')
logging.info('fuel data url: %s' % url)

logging.info('getting fuel data')
req = requests.get(url)
out = io.BytesIO(req.content)
fuel = pandas.read_excel(out, sheet_name='Data', skiprows=7, usecols='A,B,E,F', names=['date', 'petrol', 'duty', 'vat'])

fuel['year'] = pandas.DatetimeIndex(fuel['date']).year
fuel['month'] = pandas.DatetimeIndex(fuel['date']).month
petrol = fuel.groupby(['year','month'])['petrol'].mean()

data = []
for ym in petrol.keys():
    year, month = ym
    year = int(year)
    month = int(month)
    date = datetime.datetime(year, month, 1)
    value = petrol[ym]
    data.append([date, value])
petrol = pandas.DataFrame(data, columns=['date', 'petrol'])
petrol = petrol[:-1]
logging.info('petrol data:\n %s' % petrol)

duty = fuel.groupby(['year','month'])['duty'].mean()
data = []
for ym in duty.keys():
    year, month = ym
    year = int(year)
    month = int(month)
    date = datetime.datetime(year, month, 1)
    value = duty[ym]
    data.append([date, value])

duty = pandas.DataFrame(data, columns=['date', 'duty'])
duty = duty[:-1]
logging.info('duty data:\n %s' % duty)

vat = fuel.groupby(['year','month'])['vat'].mean()
data = []
for ym in vat.keys():
    year, month = ym
    year = int(year)
    month = int(month)
    date = datetime.datetime(year, month, 1)
    value = vat[ym]
    data.append([date, value])
vat = pandas.DataFrame(data, columns=['date', 'vat'])
vat = vat[:-1]
logging.info('petrol vat:\n %s' % vat)

logging.info('getting inflation data')
#https://www.ons.gov.uk/economy/inflationandpriceindices/timeseries/l55o/mm23
#CPIH ANNUAL RATE 00: ALL ITEMS 2015=100
url = 'https://www.ons.gov.uk/generator?format=csv&uri=/economy/inflationandpriceindices/timeseries/l55o/mm23'
req = requests.get(url)
data = io.StringIO(req.content.decode())
inflation = pandas.read_csv(data, names=['year', 'inflation'], skiprows=8)

inflation = inflation[inflation.year.str.isdigit()]
inflation[['year', 'inflation']] = inflation[['year', 'inflation']].astype(float)
inflation = inflation[inflation.year >= 2003]
inflation = inflation.iloc[::-1]

data = []
cum = 0
for index, row in inflation.iterrows():
    cum = cum + row['inflation']
    for month in range(1,13):
        date = datetime.datetime(int(row['year']), month, 1)
        data.append([date, cum])

for month in range(1,datetime.date.today().month):
    date = datetime.datetime(datetime.date.today().year, month, 1)
    data.append([date, 0])

inflation = pandas.DataFrame(data, columns=['date', 'inflation'])
inflation = inflation.iloc[::-1]
inflation = inflation[(inflation['date'] >= '2003-06-01')]
logging.info('petrol inflation:\n %s' % inflation)

logging.info('merging data')
dfs = [brent, usd2gbp, petrol, duty, vat, inflation]
prices = functools.reduce(lambda left, right: pandas.merge(left, right, on='date'), dfs)

prices = prices.astype({'brent': float, 'usd2gbp': float, 'petrol': float, 'duty': float, 'vat': float, 'inflation': float})
logging.info('prices:\n %s' % prices)

logging.info('saving to prices.csv')
prices.to_csv('content/prices.csv')

logging.info('calculating')
p = prices
p['brentgbp'] = p['brent'] / p['usd2gbp']
p['brentgbpadj'] = p['brentgbp'] * (p['inflation'] / 100 + 1)

p['petrolexvat'] = p['petrol'] / (p['vat'] / 100 + 1)
p['petrolexvatexduty'] = p['petrolexvat'] - p['duty'] 

p['dutyadj'] = p['duty'] * (p['inflation'] / 100 + 1)

p['petrolexvatadj'] = p['petrolexvat'] * (p['inflation'] / 100 + 1)
p['petrolexvatexdutyadj'] = p['petrolexvatexduty'] * (p['inflation'] / 100 + 1)

p['totaltax'] = p['petrol'] - p['petrolexvatexduty']
p['totaltaxadj'] = p['totaltax'] * (p['inflation'] / 100 + 1)

logging.info('saving to prices-processed.csv')
p.to_csv('content/prices-processed.csv')

logging.info('creating graphs')

matplotlib.pyplot.rcParams['figure.figsize'] = [18, 10]
matplotlib.pyplot.rcParams['figure.autolayout'] = True

fig, ax = matplotlib.pyplot.subplots()

# Brent
matplotlib.pyplot.title('Europe brent spot price FOB (USD per barrel) over time')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('Europe brent spot price FOB (USD per barrel)')
ax.scatter(p.date, p.brent)
ax.set(ylim=(0))
matplotlib.pyplot.savefig('content/brent.png')

fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Europe brent spot price FOB (GBP per barrel) over time')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('Europe brent spot price FOB (GBP per barrel)')
ax.scatter(p.date, p.brentgbp)
ax.set(ylim=(0,100))
matplotlib.pyplot.savefig('content/brentgbp.png')

fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Europe brent spot price FOB (GBP per barrel) over time adjusted for inflation')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('Europe brent spot price FOB (GBP per barrel) adjusted for inflation')
ax.scatter(p.date, p.brentgbpadj)
ax.set(ylim=(0,100))
matplotlib.pyplot.savefig('content/brentgbpadj.png')

# Petrol
fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Ultra low sulphur unleaded petrol pump price (p/litre)')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('Ultra low sulphur unleaded petrol pump price (p/litre)')
ax.scatter(p.date, p.petrol)
ax.set(ylim=(0))
matplotlib.pyplot.savefig('content/petrol.png')

fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Ultra low sulphur unleaded petrol pump price (p/litre) excluding vat')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('Ultra low sulphur unleaded petrol pump price (p/litre) excluding vat')
ax.scatter(p.date, p.petrolexvat)
ax.set(ylim=(0))
matplotlib.pyplot.savefig('content/petrolexvat.png')

fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Ultra low sulphur unleaded petrol pump price (p/litre) excluding vat and excluding duty')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('Ultra low sulphur unleaded petrol pump price (p/litre) excluding vat and excluding duty')
ax.scatter(p.date, p.petrolexvatexduty)
ax.set(ylim=(0))
matplotlib.pyplot.savefig('content/petrolexvatexduty.png')

# VAT
fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('VAT over time')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('VAT')
ax.scatter(p.date, p.vat)
ax.set(ylim=(0))
matplotlib.pyplot.savefig('content/vat.png')

# USD2GBP
fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('USD to GBP over time')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('USD to GBP')
ax.scatter(p.date, p.usd2gbp)
ax.set(ylim=(0))
matplotlib.pyplot.savefig('content/usd2gbp.png')

# Duty
fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Ultra low sulphur unleaded petrol duty rate (p/litre)')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('Ultra low sulphur unleaded petrol duty rate (p/litre)')
ax.scatter(p.date, p.duty)
ax.set(ylim=(0,80))
matplotlib.pyplot.savefig('content/duty.png')

fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Ultra low sulphur unleaded petrol duty rate (p/litre) adjusted for inflation over time')
matplotlib.pyplot.xlabel('Date')
matplotlib.pyplot.ylabel('Ultra low sulphur unleaded petrol duty rate (p/litre) adjusted for inflation')
ax.scatter(p.date, p.dutyadj)
ax.set(ylim=(0,80))
matplotlib.pyplot.savefig('content/dutyadj.png')

# Brent vs petrol
fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Europe brent spot price FOB (GBP per barrel) vs ultra low sulphur unleaded petrol pump price (p/litre)')
matplotlib.pyplot.xlabel('Europe brent spot price FOB (GBP per barrel)')
matplotlib.pyplot.ylabel('Ultra low sulphur unleaded petrol Pump price (p/litre)')

#http://home.kpn.nl/panhu001/RGB_Color_Codes/Rainbow.html
palet = [ '#0000FF',
         '#0000FF', '#0033FF', '#0066FF', '#0099FF', '#00CCFF', '#00FFFF',
         '#00FFCC', '#00FF99', '#00FF66', '#00FF33', '#00FF00', 
         '#33FF00', '#66FF00', '#99FF00', '#CCFF00', '#FFFF00',
         '#FFCC00', '#FF9900', '#FF6600', '#FF3300', '#FF0000', '#FF0000', 
         '#FF0000']

year=0
c=[]
patches=[]

patches.append(matplotlib.patches.Patch(color=palet[year], label='2003'))
for n in range(7):
    c.append(palet[year])

year = year + 1
for n in range(2004, datetime.date.today().year):
    for m in range(12):
        c.append(palet[year])
    patches.append(matplotlib.patches.Patch(color=palet[year], label=n))
    year = year + 1

patches.append(matplotlib.patches.Patch(color=palet[year], label=datetime.date.today().year))
for n in range(datetime.date.today().month - 1):
    c.append(palet[year])

ax.scatter(p.brentgbp, p.petrol, c=c)
ax.set(ylim=(0))
ax.legend(handles=patches)

matplotlib.pyplot.savefig('content/brentvspetrol.png')



fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Europe brent spot price FOB (GBP per barrel) adjusted for inflation vs ultra low sulphur unleaded petrol pump price (p/litre) excluding vat and excluding duty adjusted for inflation')
matplotlib.pyplot.xlabel('Europe brent spot price FOB (GBP per barrel) adjusted for inflation')
matplotlib.pyplot.ylabel('Ultra low sulphur unleaded petrol Pump price (p/litre) excluding vat and excluding duty adjusted for inflation')

year=0
c=[]
patches=[]

patches.append(matplotlib.patches.Patch(color=palet[year], label='2003'))
for n in range(7):
    c.append(palet[year])

year = year + 1
for n in range(2004, datetime.date.today().year):
    for m in range(12):
        c.append(palet[year])
    patches.append(matplotlib.patches.Patch(color=palet[year], label=n))
    year = year + 1

patches.append(matplotlib.patches.Patch(color=palet[year], label=datetime.date.today().year))
for n in range(datetime.date.today().month - 1):
    c.append(palet[year])

ax.scatter(p.brentgbpadj, p.petrolexvatexdutyadj, c=c)
ax.set(ylim=(0))
ax.legend(handles=patches)

matplotlib.pyplot.savefig('content/brentvspetroladj.png')


p = p[(p['date'] >= '2020-01-01')]

fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.title('Europe brent spot price FOB (GBP per barrel) adjusted for inflation vs ultra low sulphur unleaded petrol pump price (p/litre) excluding vat and excluding duty adjusted for inflation 2020-2022')
matplotlib.pyplot.xlabel('Europe brent spot price FOB (GBP per barrel) adjusted for inflation')
matplotlib.pyplot.ylabel('Ultra low sulphur unleaded petrol Pump price (p/litre) excluding vat and excluding duty adjusted for inflation')

ax.scatter(p.brentgbpadj, p.petrolexvatexdutyadj)
ax.set(xlim=(0,120))
ax.set(ylim=(0,120))

z = numpy.polyfit(p.brentgbpadj, p.petrolexvatexdutyadj, 1)
pol = numpy.poly1d(z)
ax.plot(p.brentgbpadj,pol(p.brentgbpadj),"r-")

f = 'y=%.6fx+%.6f' % (z[0], z[1]) 
ax.legend(handles = [ matplotlib.patches.Patch(color='red', label=f) ])

matplotlib.pyplot.savefig('content/brentvspetroladj2020.png')

